<?php
session_start();
if(!isset($_SESSION['kod']))
{
	?>
		<script type="text/javascript">
		redirectTime = "10";
		redirectURL = "index.php";
		setTimeout("location.href=redirectURL", redirectTime);
		</script>
	<?php 
}
else
{
	include 'models/dankoff.php';
	$user_email = $_SESSION['email'];
}
?>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="bootstrapformhelpers/css/bootstrap-formhelpers.min.css">
		<link rel="stylesheet" type="text/css" href="css/dankoff.css">	
		<link rel="stylesheet" type="text/css" href="css/attend.css">	
		<script type="text/javascript" src="js/jquery.js"></script>
		<script type="text/javascript" src="js/dankoff.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="bootstrapformhelpers/js/bootstrap-formhelpers.js"></script>
		<script type="text/javascript" src="js/utility.js"></script>
		<script type="text/javascript" src="js/resourses.js"></script>
		<title>Monitoring System</title>
	</head>
	<body style="background-color:#cccccc;">
		<div class="black">
			<div class="myheader">
				 <div class="logo"><img src="img/log.jpg" width="45px"></div>
				<div class="hwords"><h2>KOFORIDUA POLY MONITORING SYSTEM</h2></div> 

		    </div>
		</div>

		<div class="container inner_back">
			<div class="row">
				<div class="span12">
					<div class="uldiv">
						<ul class="nav nav-pills" style="margin-left:100px">
							<li><a href="#ayear" data-toggle="tab">Academic Year</a></li>
							<li><a href="#course" data-toggle="tab">Course</a></li>
							<li><a href="#lectures" id="lec" onclick="CallCourses('cid')" data-toggle="tab">Lecturers</a></li>
							<li><a href="#venue" data-toggle="tab">Venue</a></li>
							<li><a href="#classes" data-toggle="tab">Class</a></li>
							<li><a href="#ta" data-toggle="tab">Teching Assistant</a></li>
							<li><a href="#sessions" id="session" data-toggle="tab">Sessions</a></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle"data-toggle="dropdown"><?php echo $user_email; ?>
								<b class="caret"></b>
							</a>
								<ul class="dropdown-menu">
									<li><a href="#create_users" data-toggle="tab">Create USer</a></li>									
									<li><a href="#" onclick="link('LOGOUT','','kod','g','','models/dankoff.php')">Logout</a></li>
								</ul>
						    </li>
						</ul>
					</div>
					<div id="kod"></div>
