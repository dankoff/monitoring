<?php
	include 'views/header.php';
?>	
<div id="getoutofhere"></div>




<div class="container attcon">
	<!-- <div class="row">
		<div class="span12">
			<div class="attop">
				<a class="pull-right alogout" href="#logout" onclick="link('LOGOUT','','getoutofhere','g','','models/dankoff.php')">logout</a>
			</div>
		</div>
	</div> -->

	<div class="row">
		<div class="span12">
			<div class="atfom">
					<h3>Student Tally</h3>
				<form id="atform" name="atform">				

						<table>
							<tr>
								<td>Class Name : </td><td>
									<select id="atclassname" name="atclassname">
										<!-- <option value="-1">Select Class</option> -->
										<?php GETCLASSNAME(); ?>
									</select>
								</td>								
							</tr>

							<tr>
								<td>Course : </td><td>
									<select id="atcoursecode" name="atcoursecode">
										<!-- <option value="-1">Select Course</option> -->
										<?php GETCOURSES(); ?>
									</select>

								</td>								
							</tr>

							<tr>
								<td>Lecturer : </td><td><select id="atlecturer" name="atlecturer">
									<!-- <option value="-1">Select Lecturer</option> -->
									<?php GETLECTURER(); ?>
								</select></td>								
							</tr>

							<tr>
								<td>Started At : </td>
								<td>
									<select id="atstarthour"></select>
									<select id="atstartminute"></select>
									<!-- <select id="atstartwhen"></select> -->
									<input type="text" id="atstarttime" readonly="readonly" name="atstarttime" placeholder="Time Lecture Started." />
									<!-- <input type="hidden" id="lecture_start_time" name="lecture_start_time" /> -->
								</td>
							</tr>

							<tr>
								<td>Ended At : </td>
								<td>
									<select id="atendhour"></select>
									<select id="atendminute"></select>
									<!-- <select id="atendwhen"></select> -->
									<input type="text" id="atendtime" name="atendtime" readonly="readonly" placeholder="Time Lecture Ended." />
									
								</td>
							</tr>
									<input type="hidden" id="lecture_duration" name="lecture_duration" />
							<tr>
								<td>Lecturer Attended : </td>
								<td>
									<input type="checkbox" id="atlecturer_attended" name="atlecturer_attended"/>
									<span id="atlecturer_attended_text">no</span>
									<input type="hidden" id="atlecturer_attended_value" value='0' name="atlecturer_attended_value"/>
								</td>
							</tr>

							<tr>
								<td>Venue Changed : </td>
								<td>
									<input type="checkbox" id="atvenuechanged" name="atvenuechanged"/>
									<span id="atvenuechanged_text">no</span> 	
									<input type="hidden" id="atvenuechanged_value" value='0'  name="atvenuechanged_value"/>								
								</td>
							</tr>
							<tr>
								<td>Average Students Present : </td>
								<td>
									<input type="checkbox" id="atstudentspresent"  name="atstudentspresent"/>
									<span id="atstudentspresent_text">no</span>
									<input type="hidden" id="atstudentspresent_value" value='0'  name="atstudentspresent_value"/>
								</td>
							</tr>
							<tr>
								<td>Lecture Rescheduled : </td>
								<td>
									<input type="checkbox" id="atlecture_rescheduled" name="atlecture_rescheduled"/> 
									<span id="atlecture_rescheduled_text">no</span>
									<input type="hidden" id="atlecture_rescheduled_value" value='0'  name="atlecture_rescheduled_value"/>
								</td>
							</tr>
							<tr>
								<td>Was TA Used : </td>
								<td>
									<input type="checkbox" id="atlecturer_useTA" name="atlecturer_useTA"/> 
									<span id="atlecturer_useTA_text">no</span>
									<input type="hidden" id="atlecturer_useTA_value" value='0'  name="atlecturer_useTA_value"/>
									<select id="atTA" name="atTA">
										<!-- <option value="-1">Select Teaching Assistant</option> -->
										<?php GETTA(); ?>
									</select>									
								</td>
							</tr>
							<tr>
								<td>Remarks : </td>
								<td>
									<textarea id="atcomment" name="atcomment" style="min-height:50px; max-height:60px;"></textarea>
								</td>
							</tr>
							<tr><td></td>
								<td>
									<!-- <button type="button" id="btnSaveAttendance" name="btnSaveAttendance" class="btn btn-primary">Save Attendance</button> -->
									<a href="#confirmation" role="button" onclick="displayValues()" data-toggle="modal" class="btn btn-primary">Save Attendance</a>
								</td>
							</tr>
						</table>

						<script type="text/javascript">
							function displayValues()
							{								
								$("#cn").html($('#atclassname').val());
								$("#c").html($('#atcoursecode').val());
								$("#l").html($('#atlecturer').val());

								$("#st").html($('#atstarttime').val());
								$("#et").html($('#atendtime').val());

								if($('#atlecturer_attended_value').val() == "1")
								{
									$("#la").html("Yes");
								}
								else 
								{
									$("#la").html("No");									
								}
								if($('#atvenuechanged_value').val()=='1')
								{
									$("#vg").html("Yes");
								}
								else
								{
									$("#vg").html("No");									
								}
								 if($('#atstudentspresent_value').val()=='1')
								{
									$("#asp").html("Yes");
								}
								else
								{
									$("#asp").html("No");									
								}
								 if($('#atlecture_rescheduled_value').val()=='1')
								{
									$("#lr").html("Yes");
							    }
								else
								{
									$("#lr").html("No");									
								}
								 if($('#atlecturer_useTA_value').val() =="1")
								{
									$("#wtu").html("Yes");
									$('#tan').html($('#atTA').val());
									//$('#tad').css("display","black");
								}	
								else
								{									
									$("#wtu").html("No");
								}							
								$("#rmk").html($('#atcomment').val());
							}

							$(function(){
								hourtext('atstarthour','atendhour');
								minutetext('atstartminute','atendminute');
								//whentext('atstartwhen','atendwhen');

								$('#atstarthour').on('change', function(){ 
									formTime('atstarthour','atstartminute','atstartwhen','atstarttime'); 
									calDuration();
								});
								$('#atstartminute').on('change', function(){ 
									formTime('atstarthour','atstartminute','atstartwhen','atstarttime'); 
									calDuration();
								});
								//$('#atstartwhen').on('change', function(){ formTime('atstarthour','atstartminute','atstartwhen','atstarttime'); });

								$('#atendhour').on('change', function(){ 
									formTime('atendhour','atendminute','atendwhen','atendtime'); 
									calDuration();
								});
								$('#atendminute').on('change', function(){ 
									formTime('atendhour','atendminute','atendwhen','atendtime'); 
									calDuration();
								});
								//$('#atendwhen').on('change', function(){ formTime('atendhour','atendminute','atendwhen','atendtime'); });


								$('#atlecturer_attended').on('change', function(){												
									checkchange('atlecturer_attended','atlecturer_attended_text','atlecturer_attended_value');
								});
								$('#atvenuechanged').on('change', function(){												
									checkchange('atvenuechanged','atvenuechanged_text','atvenuechanged_value');
								});
								$('#atstudentspresent').on('change', function(){												
									checkchange('atstudentspresent','atstudentspresent_text','atstudentspresent_value');
								});
								$('#atlecture_rescheduled').on('change', function(){												
									checkchange('atlecture_rescheduled','atlecture_rescheduled_text','atlecture_rescheduled_value');
								});
								
								$('#atlecturer_useTA').on('change', function(){												
									checkchange('atlecturer_useTA','atlecturer_useTA_text','atlecturer_useTA_value');
									if($('#atlecturer_useTA_text').html() === 'yes'){
										$('#atTA').css({'display':'block', 'margin-top':10});
									}else{
										$('#atTA').css('display','none');
									}
								});
								$('#atTA').css('display','none');

								$('#btnSaveAttendance').on('click', function(){
									link('saveAttendance','','attsavediv','p','atform','models/dankoff.php');
									setTimeout(function(){
										window.location.reload(true);
									},1000)
								});
							});
							var checkchange = function(chkId, spnId, resId){											
								if($('#'+ chkId).prop('checked')){
									$('#' + spnId).html('yes');
									$('#' + resId).val(1);
								}
								else{
									$('#' + spnId).html('no');
									$('#' + resId).val(0);
								}
							};
							var hourtext = function(){
								var option;
								for(var i=0; i<arguments.length; i++){
									option = "<option value=''>hh</option>";
									for(var j=0; j<24; j++)
									{
										option += "<option value='"+j+"'>"+formatnum(j)+"</option>";
									}
									$('#'+ arguments[i]).html(option);
								}
							};
							var minutetext = function(){
								var option;
								for(var i=0; i<arguments.length; i++){
									option = "<option value=''>mm</option>";
									for(var j=0; j<60; j++)
									{
										option += "<option value='"+j+"'>"+formatnum(j)+"</option>";
									}
									$('#'+ arguments[i]).html(option);
								}
							};
							var whentext = function(){
								var option;
								for(var i=0; i<arguments.length; i++){
									option = "";
									option += "<option value='am'>am</option>";
									option += "<option value='pm'>pm</option>";									
									$('#'+ arguments[i]).html(option);
								}
							};
							var formatnum = function(number){
								if(number.toString().length > 1)
									return number;
								else
									return '0' + number;
							};

							var onStartHourSelect = function(){
								formTime('atstarttime');
							};

							var onStartMinuteSelect = function(){
								formTime('atstarttime');
							};

							var onStartWhenSelect = function(){
								formTime('atstarttime');
							};	

							var formTime = function(hid,mid,wid, conId)
							{
								var h = $('#' + hid + ' option:selected').val();
								var m = $('#' + mid + ' option:selected').val();
								//var w = $('#' + wid + ' option:selected').val();

								var time = '';

								if(h != -1 && m != -1)
								{
									if(h <= 12){
										time =  formatnum(h) + ':' + formatnum(m) + ' am';
									}
									else{
										time =  formatnum(h - 12) + ':' + formatnum(m) + ' pm';
									}

									$('#' + conId).val(time);
								}
								else{									
									$('#' + conId).val(time);
								}

								//calDuration();
							};

							var calDuration = function()
							{
								var sh = $('#atstarthour option:selected').val(), eh = $('#atendhour option:selected').val();
								var sm = $('#atstartminute option:selected').val(), em = $('#atendminute option:selected').val();
								//var sw = $('#atstartwhen option:selected').val(), ew = $('#atendwhen option:selected').val();
																
								var dur_h = 0, dur_m = 0, h, m;
								if(sh!='' && eh!=''){
									if(parseInt(sh) > parseInt(eh)){
										pop('atstarthour','Error','The start hour is greater than end hour. please check.');
										return false;
									}

									h = eh - sh; console.log('hour dura : ' + h);
									m = em - sm; console.log('minute dura : ' + m);

									if(h==0 && m >= 0){
										dur_h = h; dur_m = m;
										$('#lecture_duration').val(dur_m + ' min')
										//return false;
									}
									else if(h==0 && m<0){
										return false;
									}
									else if(h>0 && m>=0){
										dur_h = h; dur_m = m;
										$('#lecture_duration').val(dur_h + ' hrs ' + dur_m + ' min');
									}
									else if(h>0 && m<0){
										var him = (h * 60) + m; //console.log('hours in minutes : ' + him);
										dur_h = (him / 60).toString().substring(0, (him / 60).toString().indexOf('.'));//console.log('hour : ' + dur_h);
										dur_m = him % 60; //console.log('minutes : ' + dur_m);
										$('#lecture_duration').val(dur_h + ' hrs ' + dur_m + ' min');
									}
									console.log($('#lecture_duration').val());
								}
							};	
							var pop = function(tagid,title,info){
								$('#'+ tagid).popover({
									trigger:'manual',
									title:title,
									content:info
								});

								$('#'+tagid).focus();
								$('#'+tagid).popover('show');
								

								setTimeout(function(){
									$('#'+tagid).popover('hide');
								},5000);
								
							}

						</script>
					
				</form>
				
			</div>

			<div id="attsavediv"></div>
		</div>		
	</div>
			<!-- Let Model Something and See -->
		<div class="modal hide fade" id="confirmation"  tabindex="-1" role="dialog" aria-hidden="true" >
		  	<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    <h3>Confirmation of Information</h3>
		  	</div>
	  		<div class="modal-body">
	    		<div>
	    			<table id="modal_table" style="width:90%; margin:0 auto">
	    				<tbody>
	    					<tr>
	    						<td><label>Class Name:</label></td><td><span style="font-size:18px;color:blue" id="cn"></span></td>
	    					</tr>
	    					<tr>
	    						<td><label>Course:</label></td><td><span style="font-size:18px;color:blue" id="c"></span></td>
	    					</tr>
	    					<tr>
	    						<td><label>Lecturer:</label></td><td><span style="font-size:18px;color:blue" id="l"></span></td>
	    					</tr>
	    					<tr>
	    						<td><label>Started At:</label></td><td><span style="font-size:18px;color:blue" id="st"></span></td>
	    					</tr>
	    					<tr>
	    						<td><label>Ended At:</label></td><td><span style="font-size:18px;color:blue" id="et"></span></td>
	    					</tr>
	    					<tr>
	    						<td><label>Lecturer Attended:</label></td><td><span style="font-size:18px;color:blue" id="la"></span></td>
	    					</tr>
	    					<tr>
	    						<td><label>Venue Changed:</label></td><td><span style="font-size:18px;color:blue" id="vg"></span></td>
	    					</tr>
	    					<tr>
	    						<td><label>Average Students Present:</label></td><td><span style="font-size:18px;color:blue" id="asp"></span></td>
	    					</tr>
	    					<tr>
	    						<td><label>Lecture Rescheduled:</label></td><td><span style="font-size:18px;color:blue" id="lr"></span></td>
	    					</tr>
	    					<tr>
	    						<td><label>Was TA Used:</label></td><td><span style="font-size:18px;color:blue" id="wtu"></span></td>
	    					</tr>
	    					<tr id="tad">
	    						<td>Name of TA:</td><td><span style='font-size:16px;color:blue' id='tan'></span></td>
	    					</tr>	
	    					<tr>
	    						<td><label>Remakrs:</label></td><td><span style="font-size:18px;color:blue" id="rmk"></span></td>
	    					</tr>
	    				</tbody>	
	    			<table>	  
	    			 			
	    		</div>
	  		</div>
	  		<div class="modal-footer">
	    		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
	    		<button  class="btn btn-primary" id="btnSaveAttendance" data-dismiss="modal">Done</button>
	  		</div>
		</div>

</div>
