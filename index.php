<?php
include 'views/login_header.php';
?>
		<div class="login_style">
			<div id="login_feed_con">
     		<button type="button" class="close" data-dismiss="alert"></button>
     		<span id="login_feed"></span>
     		</div>
     		<br/>
			<form class="form-horizontal" id="login_form" method="post" style="border:0px solid red; width:80%;margin:0 auto">
			<div class="control-group">
				<label class="control-label" for="inputEmail">Username</label>
				<div class="controls">
					<input type="text" id="inputEmail" style="height: 30px;width: 300px" name="email" placeholder="Email">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">Password</label>
				<div class="controls">
					<input type="password" id="inputPassword" style="height: 30px;width: 300px" name="password" placeholder="Password">
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<label class="checkbox"> <input type="checkbox"> Remember me
					</label>
					<button type="button" class="btn" id="signin">Sign in</button>
				</div>
			</div>
			</form>
		<br/>
	</div>
		<script type="text/javascript">
		$(document).ready(function(){
			//$('#login_feed_con').css("display","none");
			$('#signin').click(do_Login);
			});
		function showResult(info,status)
        {
        	$('#login_feed').html(info);
        	$('#login_feed').addClass(status);
          	$('#login_feed_con').css('display','block');
          	
      		setTimeout(function()
			{							
				$("#login_feed").html("");
				$("#login_feed").removeClass(status);
				$("#login_feed_con").css('display','none');
			},2000);
        }
		function do_Login()
		{
			var user = $('#inputEmail').val();
			var ps = $('#inputPassword').val();

			if(user !="" && ps !="")
			{
				link("LOGIN", "", "login_feed", "p", "login_form", "Models/dankoff.php");
				setTimeout(function(){
					$('#login_feed').html("");
				},2000)
			}
			else
			{
				showResult("Please All Fields Are Required", "error");
			}
		}
		
		</script>
		</body>
			
</html>
