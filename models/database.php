<?php
function CreateDatabaseTables()
{
	$cont = mysqli_connect('localhost','root','');
	if(mysqli_connect_errno())
	{
	}	
	 $database_name = 'CREATE DATABASE IF NOT EXISTS kpolymodb';
	 mysqli_query($cont,$database_name);
}

function CreateTabe()
{
			$conn = mysqli_connect("localhost","root","","kpolymodb");
			//creating table login
			$login = "CREATE TABLE IF NOT EXISTS login(
			id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
			apikey VARCHAR(10),
			name VARCHAR(255),
			fon VARCHAR(20),
			email VARCHAR( 100 ) ,
			password VARCHAR( 100 ),
		    level INT(2),
			active INT(2),
			createdDate VARCHAR(20),
	         updatedDate VARCHAR(20)
				)ENGINE=InnoDB";
	         mysqli_query($conn,$login);	         
	         	         
	         //CREATE USER TABLE
	         /*$user = "CREATE TABLE IF NOT EXISTS users(
	         		id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	         		apikey VARCHAR(10),
	         		name VARCHAR(100),
	         		email VARCHAR(20),
	         		fon VARCHAR(20),
	         		active INT(2),
	         		createdDate VARCHAR(20),
	         		updatedDate VARCHAR(20)	    		
	         		)ENGINE=InnoDB";
	         mysqli_query($conn,$user); */

	         //COURSE
	         $COURSE = "CREATE TABLE IF NOT EXISTS course(
	         			id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	         			courseCode VARCHAR(20),
	         			courseName VARCHAR(255),
	         			level VARCHAR(50),
	         			createdDate VARCHAR(20),
	         			updatedDate VARCHAR(20)
	         			)ENGINE=InnoDB";
	         mysqli_query($conn,$COURSE);

	         //LECTURERS
	         $LECTURERS ="CREATE TABLE IF NOT EXISTS lecturers(
	         				id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	         				LecturerName VARCHAR(50),
	         				LecturerID VARCHAR(20),
	         				dept VARCHAR(50),
	         				courseName VARCHAR(255),
	         				createdDate VARCHAR(20),
	         				updatedDate VARCHAR(20)
	         				)ENGINE=InnoDB";
				mysqli_query($conn,$LECTURERS);

			//VENUE
				$VENUE = "CREATE TABLE IF NOT EXISTS venue(
						 id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
						 venueID VARCHAR(20),
						 venueName VARCHAR(50),
						 createdDate VARCHAR(20),
	         			 updatedDate VARCHAR(20)
						)ENGINE=InnoDB";
				mysqli_query($conn,$VENUE);

				//Class
				$class = "CREATE TABLE IF NOT EXISTS classes(
						id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
						className VARCHAR(50),
						level VARCHAR(50),
						createdDate VARCHAR(20),
	         			updatedDate VARCHAR(20)
					)ENGINE=InnoDB";
				mysqli_query($conn,$class);

				//Teaching Assistant
				$ta = "CREATE TABLE IF NOT EXISTS ta_table(
						id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
						taid VARCHAR(50),
						taName VARCHAR(50),
						createdDate VARCHAR(20),
	         			updatedDate VARCHAR(20)
						)ENGINE=InnoDB";
				mysqli_query($conn,$ta);

				//Sessions
				$Sessions = "CREATE TABLE IF NOT EXISTS sess_table(
						id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
						academicYear VARCHAR(20),
						semester VARCHAR(20),
						lectID VARCHAR(255),
						courseid VARCHAR(20),
						classname VARCHAR(50),
						vanue VARCHAR(20),
						creditHours VARCHAR(50),
						createdDate VARCHAR(20),
	         			updatedDate VARCHAR(20)
						)ENGINE=InnoDB";
				mysqli_query($conn,$Sessions);	

				//Academic Year
				$ar = "CREATE TABLE IF NOT EXISTS ayear(
						id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,						
						ayear VARCHAR(50),
						semester VARCHAR(10),
						createdDate VARCHAR(20),
	         			updatedDate VARCHAR(20)
						)ENGINE=InnoDB";
				mysqli_query($conn,$ar); 

				//Attendance
				   $ar = "CREATE TABLE IF NOT EXISTS attendance(
						id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
						acyear VARCHAR(50),
						semester TINYINT(1),						
						ddate VARCHAR(50),
						classname VARCHAR(50),
						courseCode VARCHAR(50),
						lecturerID VARCHAR(50),
						starttime VARCHAR(50),
						endingtime VARCHAR(50),
						duration VARCHAR(50),
						attended TINYINT(1),
						venueChange BIT(1),
						studentPresent BIT(1),
						rescheduled BIT(1),
						TAUsed TINYINT(1),
						TaID VARCHAR(50),
						comment VARCHAR(1000),
						createdDate VARCHAR(20),
	         			updatedDate VARCHAR(20)
						)ENGINE=InnoDB";
				mysqli_query($conn,$ar);      	
}
CreateDatabaseTables();
CreateTabe();


?>