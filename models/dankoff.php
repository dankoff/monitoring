<?php
// STANDARD PACKAGE //
define('DB_NAME','kpolymodb',true);
define('DB_HOST','localhost',true);
define('DB_PASS','',true);
define('DB_USER','root',true);

//define('COLDSIS_ROOT','',true);
//define("C_URL","http://".$_SERVER['SERVER_NAME']."/DEO_System/",false);

initialize(); 
//sync_sessions();
send_data();

function initialize(){
     @session_start();
     include 'Lab.php';
     include 'database.php';
     include 'Settings.php';
     include 'Users.php';
     include 'Attend.php';
     //include 'Teachers.php';
     include 'getFunction.php';
     //include 'Enrolment.php';
     //include '../views/dankoff_views.php';
 }

function reload(){
     run('logout'); header("Location: ".$_SERVER['PHP_SELF']);	
}

function send_data(){
if(isset($_FILES['file'])) $_SESSION['files'] .= file_upload().';'.$_SESSION['files'];
if(isset($_GET['q'])): $cmd = $_GET['q'];
kill_session($cmd);
echo (!function_exists($cmd)) ? run($cmd,post_path()) : run('nully',post_path()).$cmd();	
endif;
 }
function post_path(){
if(isset($_SESSION['files'])):
$x = 'files='.$_SESSION['files'];
unset($_SESSION['files']);
else: 
$x = '';
endif;
$array = array_merge($_POST,$_GET);
foreach($array as $key => $value):
if($key !== 'q') $x .= ','.urlencode($key).'='.urlencode($value);
endforeach;	
return $x;
 }
function run($cmd,$params=''){
set_time_limit(0);
$str = $_SESSION['path'].'&q='.$cmd.'&'.str_replace(',','&',str_replace(' ','%20',$params));
kill_session($cmd);
return ($cmd !== 'openFile') ? @file_get_contents($str) : $str;
 }
function kill_session($lgt){
if($lgt == 'logout') session_destroy();
	}
function value($x='',$y=''){
if(!empty($x)) return $x;
if(isset($_REQUEST[$y])) return $_REQUEST[$y];
 }
function options($xml,$tag,$script=''){
if($xml):
$var = array();
foreach($xml->row as $row):
$var[] = (string) $row->name.'='.$row->id;
endforeach;
sort($var);
echo '<select name="'.$tag.'" id="'.$tag.'" '.$script.'>'; 
foreach($var as $key):
list($name,$id) = explode('=',$key);
echo '<option value="'.$id.'">'.$name.'</option>';
endforeach;
echo '</select>';
endif;
	}
function xml($i){
if($i) $xml = simplexml_load_string($i);
if($xml) return $xml;
	}
function article($id){
if($id) return xml(run('fetchXML','id='.$id));	
	}
function file_upload(){
if(!file_exists(LOCAL_FOLDER)) @mkdir(LOCAL_FOLDER,0777);
$file = run('_file','file='.str_replace(' ','-s-',$_FILES['file']['name']).',rtn=filename');
if(run('file_is_safe','file='.$file) == TRUE): 
@move_uploaded_file($_FILES['file']['tmp_name'],LOCAL_FOLDER.'/'.$file);
return save_file($file);
endif;
 }
?>