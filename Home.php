<?php
include 'views/header.php';
?>
		<!-- <div class="container inner_back">
			<div class="row">
				<div class="span12">
					<div class="uldiv">
						<ul class="nav nav-pills" style="margin-left:100px">
							<li><a href="#ayear" data-toggle="tab">Academic Year</a></li>
							<li><a href="#course" data-toggle="tab">Course</a></li>
							<li><a href="#lectures" data-toggle="tab">Lecturers</a></li>
							<li><a href="#venue" data-toggle="tab">Venue</a></li>
							<li><a href="#classes" data-toggle="tab">Class</a></li>
							<li><a href="#ta" data-toggle="tab">Teching Assistance</a></li>
							<li><a href="#sessions" data-toggle="tab">Sessions</a></li>
							<li class="dropdown"><a href="#" class="dropdown-toggle"data-toggle="dropdown"><?php echo $user_email; ?>
								<b class="caret"></b>
							</a>
								<ul class="dropdown-menu">
									<li><a href="#create_users" data-toggle="tab">Create USer</a></li>									
									<li><a href="#" onclick="link('LOGOUT','','kod','g','','models/dankoff.php')">Logout</a></li>
								</ul>
						    </li>
						</ul>
					</div>
					<div id="kod"></div> -->

					<div class="tab-content" style="overflow:hidden;margin-top:-25px">
						 <div class="tab-pane" id="ayear">
						 	<div class="bigDiv">
						 			<div class="headme">ACADEMIC</div>
						 		<div class="frmAy">
						 			<div id="ayearfeed" style="margin-bottom:20px;margin-top:20px"></div>						 			
	 					 			<form id="fayear" class="form-horizontal" style="border:0px solid blue;width:70%;margin:0 auto;">
						 					<div class="control-group">
						 						<br/>
						 							<label class="control-label" for="txtay">Academic Year:</label>
						 							<div class="controls">
						 								<input type="text" id="txtay" name="txtay" style="height:30px;width:300px;">
						 							</div>
						 					</div>
						 					<div class="control-group">
						 						<br/>
						 							<label class="control-label" for="txtses">Semester:</label>
						 							<div class="controls">
						 								<select id="txtses" name="txtses" style="height:30px;width:300px;">
						 									<option value="">Select Semester</option>
						 									<option value="1">One</option>
						 									<option value="2">Two</option>
						 								</select>
						 							</div>
						 					</div>
						 					<div class="control-group">						 							
						 							<div class="controls">
						 								<button type="button" id="ayear_save" class="btn btn-success">SAVE</button>
						 								<button type="button" class="btn" onclick="CLEAR('ayear_save','fayear','SAVE')">CANCEL</button>
						 							</div>
						 							<br/>
						 					</div>
						 			</form>

						 		</div>
						 		<div class="showdata" style="margin-top:-30px;">
 						 			<div class="headme" style="width:100%">Active Academic Year </div>
 						 			<div id="ayData" class="showdata_single"></div>
 						 		</div>
						 	</div>
						 </div>
<!--............................. COURSES FORMS ........................ -->
 	<div class="tab-pane" id="course">
 		<div class="bigDiv" id="domewell"> 						 		
 		      <div class="headme" style="">COURSE</div>
 			   <div class="cform">
 				    <div id="fcourse" style=""></div>
 					<form id="courseID" class="form-horizontal">
 						<br/>
 						<div class="control-group">
 						 	<label class="control-label" for="cCode">Course Code:</label>
 						 	<div class="controls">
 						 		<input type="text" name="cCode" id="cCode" style="height:30px;width:300px;">
 						 	</div>
 						</div>
 						 <div class="control-group">
 						 	<label class="control-label" for="cname">Course Name:</label>
 						 	<div class="controls">
 						 		<input type="text" name="cname" id="cname" placeholder="Enter Course Name" style="height:30px;width:299px;">
 						 	</div>
 						 </div>
 						 <div class="control-group">
 						 	<label class="control-label" for="level">Level:</label>
 						 	<div class="controls">
 						 		<input type="text" name="level" id="level" placeholder="Enter Level" style="height:30px;width:300px;">
 						 	</div>
 						 </div>
 						 <div class="control-group">
 						 	<div class="controls">
 						 		<button id="addCourse" type="button" class="btn btn-success" data-loading-text="Loading...">SAVE</button>&nbsp;
 						 		<button type="button" class="btn" onclick="CLEAR('addCourse','courseID','SAVE')" >CANCEL</button>
 						 		<input type="text" id="ccid" name="ccid" readonly style="color:#cccccc;background-color:#cccccc;border:1px solid #cccccc;width:1px">
 						 	</div>
 						 </div> 						 				
 					</form>
 					</div>
 						<div class="showdata" style="margin-top:5px;">
 							<div class="headme" style="width:100%">Active Courses</div>
 							<div id="entData" class="showdata_single"></div>
 						</div>
 				</div>
			</div> 
<!--...................... Ending of Course.................................. -->

<!-- .........................LECTURERS FORM..................................  -->
 						 <div class="tab-pane" id="lectures">
 						 	<div class="bigDiv">
 						 			<div class="headme" style="margin-bottom:2px">LECTURERS</div>
 						 		<div class="frmAy">
 						 				<div id="lecfeed" style=""></div>
 						 			<form id="frmlect" class="form-horizontal">
 						 				<br/>
 						 				<div class="control-group">
   											 <label class="control-label" for="lname">Lecturer Name:</label>
   											 <div class="controls">
 						 						<input type="text" name="lname" id="lname" style="height:30px;width:300px;">
 						 					 </div>
 						 				</div>

 						 				<div class="control-group">
    											<label class="control-label" for="lid">Lecturer ID:</label>
    										<div class="controls">
 						 						<input type="text" name="lid" id="lid" style="height:30px;width:300px;">
 						 					</div>
 						 				</div>

 						 				<div class="control-group">
    										<label class="control-label" for="dept">Department</label>
    											<div class="controls">
 						 							<select  name="dept" id="dept" style="height:30px;width:300px;">
 						 								
 						 								<option value="Marketing">MARKETING</option>
 						 							</select>
 						 						</div>
 						 				</div>
 						 				<div class="control-group">
    										<label class="control-label" for="dept">Course</label>
    											<div class="controls">
 						 							<select  name="cid" id="cid" style="height:30px;width:300px;">
 						 								<option value="">Select Course</option>

 						 								<?php //GETCOURSES();  ?>
 						 							</select>
 						 						</div>
 						 				</div>
 						 				<div class="control-group">    										
    											<div class="controls">
 						 							<button type="button" id="l_save" class="btn btn-success">SAVE</button>&nbsp;
 						 							<button type="button" class="btn" onclick="CLEAR('l_save','frmlect','SAVE')">CANCEL</button>
 						 						<input type="text" id="idd" name="idd" style="color:#cccccc;width:1px;margin-left:-80px;margin-top:-85px;border:none;background-color:#cccccc">
 						 						</div>
 						 				</div>
 						 				<br/>
 						 				
 						 			</form>
 						 	    </div>
 						 	    	<div class="showdata" style="margin-top:-30px;height:200px">
 						 				<div class="headme" style="width:100%"><p>Active Lecturers</p></div> 						 			
 						 					<div id="lectData" class="showdata_single" style="height:190px"></div>
 						 			</div>
 						 	</div>
 						 </div>
<!--...................... Ending of Lecturers ............................ -->
<!-- ........................VENUE FORM...................................  -->
 						 <div class="tab-pane" id="venue">
 						 	<div class="bigDiv">
 						 			<div class="headme" style="margin-bottom:2px">VENUE</div>
 						 		<div class="frmAy">
 						 			<div id="vfeed" style=""></div>
 						 			<form id="idvenue" class="form-horizontal">
 						 				<br/>
 						 				<div class="control-group">
    										<label class="control-label" for="vID">VenueID:</label>
    										<div class="controls">
 						 						<input type="text" name="vID" id="vID" style="height:30px;width:300px;">
 						 					</div>
 						 				</div>
 						 				<div class="control-group">
    										<label class="control-label" for="vname">Venue Name:</label>
    										<div class="controls">
 						 						<input type="text" name="vname" id="vname" style="height:30px;width:300px;">
 						 					</div>
 						 				</div>
 						 				<div class="control-group">
 						 					<div class="controls">
											<button type="button" class="btn btn-success" id="v_save">SAVE</button>&nbsp;
											<button class="btn" type="button" onclick="CLEAR('v_save','idvenue','SAVE')" >CANCEL</button>
											</div>
 						 				</div>
 						 				<br/>	
 						 				<input type="text" id="v_id" name="v_id" style="color:#cccccc;width:1px;background-color:#cccccc">					 			
 						 			</form>
 						 		</div>
 						 			<div class="showdata">
 						 				<div class="headme" style="width:100%;margin-top:-31px"><p>Active Venues</p></div> 						 			
 						 					<div id="vData" class="showdata_single"></div>
 						 			</div>
 						 	</div>
 						</div>
<!-- Ending of Venue -->
<!-- ....................CLASSES.......................................  -->
 						 <div class="tab-pane" id="classes">
 						 	<div class="bigDiv">
 						 			<div class="headme" style="margin-bottom:2px">CLASSES</div>
 						 		<div class="frmAy">
 						 				<div id="cfeed"></div>
 						 			<form id="fclasses" class="form-horizontal">
 						 				<br/>
 						 				<div class="control-group">
    											<label class="control-label" for="classname">Classname:</label>
    										<div class="controls">
 						 						<input type="text" name="classname" id="classname" style="height:30px;width:300px;">
 						 					</div>
 						 				</div>
 						 				<div class="control-group">
    											<label class="control-label" for="clevel">Level:</label>
    										<div class="controls">
 						 						<input type="text" name="clevel" id="clevel" style="height:30px;width:300px;">
 						 					</div>
 						 				</div>
 						 				<div class="control-group">
    										<div class="controls">    											
 						 						<button type="button" class="btn btn-success" id="c_save">SAVE</button>&nbsp;
 						 						<button type="button" class="btn" onclick="CLEAR('c_save','fclasses','SAVE')" >CANCEL</button>
 						 					</div>
 						 				</div>
 						 				<br/>
 						 				<input type="text" id="cidd" name="cidd" style="color:#cccccc;width:1px;background-color:#cccccc">
 						 			</form>
 						 		</div>
 						 			<div class="showdata">
 						 				<div class="headme" style="width:100%;margin-top:-30px"><p>Active Classes</p></div> 						 			
 						 					<div id="cData" class="showdata_single"></div>
 						 			</div>
 						 	</div>
 						 </div>
<!-- Ending of Classes -->
<!-- ......................TEACHING ASSISTANCE.....................................  -->
 						 <div class="tab-pane" id="ta">
 						 	<div class="bigDiv">
 						 			<div class="headme" style="margin-bottom:2px">TEACHING ASSISTANCE</div>
 						 		<div class="frmAy">
 						 				<div id="fta"></div>
 						 	 		<form id="frmta"  class="form-horizontal">
 						 	 			<br/>
 						 	 			<div class="control-group">
    											<label class="control-label" for="taID">TA ID:</label>
    										<div class="controls">
 						 						<input type="text" name="taID" id="taID" style="height:30px;width:300px;">
 						 					</div>
 						 				</div>
 						 					<div class="control-group">
    											<label class="control-label" for="taname">TA Name:</label>
    											<div class="controls">
 						 							<input type="text" name="taname" id="taname" style="height:30px;width:300px;">
 						 						</div>
 						 					</div>
 						 					<div class="control-group">    											
    											<div class="controls">
 						 							<button type="button" class="btn btn-success" id="s_ta">SAVE</button>&nbsp;
 						 							<button type="button" class="btn" onclick="CLEAR('s_ta','frmta','SAVE')">CANCEL</button>
 						 						</div>
 						 					</div>
 						 					<br/>
 						 					<input type="text" id="dataID" name="dataID" style="width:1px;color:#cccccc;background-color:#cccccc">
 						 			</form>
 						 		</div>
 						 			<div class="showdata">
 						 				<div class="headme" style="width:100%;margin-top:-31px"><p> Active Teching Assistance</p></div> 						 			
 						 					<div id="taData" class="showdata_single"></div>
 						 			</div>
 						 	</div>
 						 </div>
<!-- Ending of TA -->
<!-- .............................SESSION CREATION..............................  -->
 						 <div class="tab-pane" id="sessions" style="height:auto;background-color:#ffffff;">
 						 	<div class="bigDiv">
 						 			<div class="headme" style="margin-bottom:2px">SESSIONS</div>
 						 		<div class="frmAy">
 						 				<div id="fses"></div>
 						 			<form id="frmSec" class="form-horizontal">
 						 				<br/>
 						 				<div class="control-group">
    											<label class="control-label" for="sayear">Academic Year:</label>
    											<div class="controls">	
 						 							<select name="sayear" id="sayear" style="height:30px;width:300px;">
 						 								<!-- <option value="">Select Academic Year</option> -->
 						 								<?php GETAY() ?>
 						 							</select>
 						 						</div>
 						 				</div>

 						 				<div class="control-group">
    											<label class="control-label" for="ayear">Semester:</label>
    											<div class="controls">	
 						 							<select name="ss" id="ss" style="height:30px;width:300px;">
 						 								<!-- <option value="">Select Semester</option> -->
 						 								<!-- <option value="1">One</option>
 						 								<option value="2">Two</option>
 						 								<option value="3">Three</option>
 						 								<option value="4">Four</option> -->
 						 							</select>
 						 						</div>
 						 				</div>

 						 				<div class="control-group">
    											<label class="control-label" for="lID">Lecturer ID:</label>
    											<div class="controls">	
 						 							<select name="lID" id="lID" style="height:30px;width:300px;">
 						 								<option value="">Select Lecturer</option>
 						 								<?php// GETLECTURER(); ?>
 						 							</select>
 						 						</div>
 						 				</div>
 						 				<div class="control-group">
    										<label class="control-label" for="ccode">Course:</label>
    											<div class="controls">	
 						 							<select name="ccode" id="ccode" style="height:30px;width:300px;">
 						 								<option value="">Select Course</option>
 						 								<?php //GETCOURSES(); ?>
 						 							</select>
 						 						</div>
 						 				</div>
 						 				<div class="control-group">
    										<label class="control-label" for="cname">Class Name:</label>
    											<div class="controls">	
 						 							<select name="scname" id="scname" style="height:30px;width:300px;">
 						 								<option value="">Select Class Name</option>
 						 								<?php //GETCLASSNAME() ?>
 						 							</select>
 						 						</div>
 						 				</div>
 						 				<div class="control-group">
    										    <label class="control-label" for="venue">Venue:</label>
    											<div class="controls">	
 						 							<select name="svenue" id="svenue" style="height:30px;width:300px;">
 						 								<option value="">Select Venue</option>
 						 								<?php //GETVENUE() ?>
 						 							</select>
 						 						</div>
 						 				</div>
 						 				<div class="control-group">
    										<label class="control-label" for="ayear">Credit Hours:</label>
    											<div class="controls">	
 						 							<input type="text" name="chours" id="chours" style="height:30px;width:300px;">						 							
 						 						</div>
 						 				</div>

 						 				<div class="control-group">    										
    											<div class="controls">	
 						 							<button type="button" class="btn btn-success" id="ses_save">SAVE</button>&nbsp;
 						 							<button type="button" class="btn" onclick="CLEAR('ses_save','frmSec','SAVE')">CANCEL</button>
 						 						</div>
 						 				</div>
 						 				<br/>
 						 				<input type="text" name="seID" id="seID" style="width:1px; color:#cccccc;background-color:#cccccc">
 						 			</form>
 						 		</div>
 						 		<div class="showdata">
 						 				<div class="headme" style="width:100%;margin-top:-31px" ><p>Active Sessions</p></div> 						 			
 						 				<div id="sesData" class="showdata_single"></div>
 						 		</div>
 						 	</div>
 						 </div>
<!--..............................CREATE USERS..................................-->
						<div class="tab-pane" id="create_users" style="height:auto;background-color:#ffffff;">
						 	<div class="bigDiv">
						 			<div class="headme">CREATE USERS</div>
						 			<div class="frmAy" style="margin-top:20px">
						 				<div id="fuser"></div>
						 				<form id="frmuser" class="form-horizontal">
 						 				<br/>
 						 				<div class="control-group">
   											 <label class="control-label" for="username">Name:</label>
   											 <div class="controls">
 						 						<input type="text" name="username" id="username" style="height:30px;width:300px;">
 						 					 </div>
 						 				</div>
 						 				<div class="control-group">
    										<label class="control-label" for="ufon">Telephone</label>
    											<div class="controls">
 						 							<input type="text" id="ufon" name="ufon" style="height:30px;width:300px;">
 						 						</div>
 						 				</div>

 						 				<div class="control-group">
    											<label class="control-label" for="uemail">Email:</label>
    										<div class="controls">
 						 						<input type="text" name="uemail" id="uemail" style="height:30px;width:300px;">
 						 					</div>
 						 				</div>
 						 				<div class="control-group">
    											<label class="control-label" for="cemail">Confirm Email:</label>
    										<div class="controls">
 						 						<input type="text" name="cemail" id="cemail" style="height:30px;width:300px;">
 						 					</div>
 						 				</div>
 						 				<div class="control-group">
    										<label class="control-label" for="ps">Password</label>
    											<div class="controls">
 						 							<input type="password" id="ups" name="ups" style="height:30px;width:300px;">
 						 						</div>
 						 				</div> 
 						 				<div class="control-group">
    										<label class="control-label" for="cps">Confirm Password</label>
    											<div class="controls">
 						 							<input type="password" id="cps" name="cps" style="height:30px;width:300px;">
 						 						</div>
 						 				</div> 						 				
 						 				 <div class="control-group">    										
    											<div class="controls">
 						 							<button type="button" id="u_save" class="btn btn-success">SAVE</button>&nbsp;
 						 							<button type="button" class="btn" onclick="CLEAR('u_save','frmuser','SAVE')">CANCEL</button>
 						 						<input type="text" id="idd" name="idd" style="color:#cccccc;width:1px;margin-left:-80px;margin-top:-85px;border:none;background-color:#cccccc">
 						 						</div>
 						 				</div>
 						 				
 						 				<br/> 						 				
 						 			</form>
						 		</div>
						 			<div class="showdata">
 						 					<div class="headme" style="width:100%;margin-top:-31px"><p>Active Sessions</p></div> 						 			
 						 					<div id="userData" class="showdata_single" ></div>
 						 			</div>
						 	</div>
						 </div>
<!--..............................CREATE USERS..................................-->
				</div>
				</div>
			</div>
			</div>
				<div id="universal"></div>






