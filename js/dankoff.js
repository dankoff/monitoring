

//another files

//memory

function createCookie(name,value,days){
if(days!==''){
	var date = new Date();
	date.setTime(date.getTime()+(days*24*60*60*1000));
	var expires = '; expires='+date.toGMTString();
 }else{
	var expires='';
 }
document.cookie = name+'='+value+expires;
 }
function readCookie(name){
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)===' '){c = c.substring(1,c.length);}
		if (c.indexOf(nameEQ) === 0){return c.substring(nameEQ.length,c.length);}
	}
	return null;
 }
function eraseCookie(name){
	createCookie(name,"stub",-1);
 }
function killCookies(){ 
var whole_cookie = document.cookie;
var each_cookie = whole_cookie.split(";");
for (i = 0; i < each_cookie.length; i++){ 
var cname = each_cookie[i].split("=");
eraseCookie(cname[0]);
 }
 }
//fade
function setOpacity(id,quantum){
		var j = getId(id);
		if(j){
		var num = parseInt(quantum);
        j.style.opacity = num;
        j.style.MozOpacity = num;
        j.style.KhtmlOpacity = num;
        j.style.filter = "alpha(opacity=" + (num * 100) + ");";
		}
      }
function move_dir(id,dir,qty){
	var j = getId(id);
	j.style.position='absolute';
	if(!readCookie(id+'_left')) createCookie(id+'_left',0,1);
	if(!readCookie(id+'_top')) createCookie(id+'_top',0,1);
	if(dir == 'left'){
		createCookie(id+'_left',parseInt(readCookie(id+'_left'))+qty,1);
		j.style.left = readCookie(id+'_left')+'px';
	}
	if(dir == 'top'){
		createCookie(id+'_top',parseInt(readCookie(id+'_top'))+qty,1);
		j.style.top = readCookie(id+'_top')+'px';
	}
	}
function fadeIn(id,steps,duration){
for (i = 0; i <= 1; i += (1 / steps)) { 
	setTimeout("setOpacity('"+id+"','"+i+"')",i*duration); 
	}
 }
function fadeOut(id,steps,duration){
for (i = 0; i <= 1; i += (1 / steps)) {
	setTimeout("setOpacity('"+id+"','"+(1-i)+"')",i*duration);
   } 
 }
//DOM
function getId(id){
	return document.getElementById(id);
	}
function getFrameId(id){
var oDoc = getId(id).contentWindow || getId(id).contentDocument;
if(oDoc.document){ return oDoc.document;}
 }
function getContent(id){
if(id){
createCookie('lastId_gc',id,1);
var j = getId(id);
	if(j.value && j.value.length > 0){
		var content = j.value;
	}else if(j.innerHTML && j.innerHTML.length > 0){
		var content = j.innerHTML;
	}
return (content) ? content : '';
}
 }
function getInput(form,element){
return getId(form).elements[element];
	}
//navigation
function GetXmlHttpObject(){
var xmlHttp=null;
		try{ xmlHttp=new XMLHttpRequest();}
		catch(e){ 
		try{ xmlHttp=new ActiveXObject('Msxml2.XMLHTTP');}
		catch(d){ xmlHttp=new ActiveXObject('Microsoft.XMLHTTP');}
		}
	return xmlHttp;
 }
function inLink(id,content){
var j = getId(id);
if(j!==null){
	j.innerHTML=content;
	j.value=content;
	}
 }
function validate(formId){ 
var qstr = '';
var xform = getId(formId);
if(xform===null){alert('Form Construct Error!'); return;}
var elem = xform.elements; 
	for(var i = 0; i < elem.length; i++){ 
	var k = getId(elem[i].name);
		if(elem[i].value == '' && elem[i].getAttribute('required') == 'yes'){
			alert('"'+elem[i].getAttribute('alias')+'" is a required field and cannot be left empty!'); 
			k.style.backgroundColor='#ffeedd'; 
			k.focus();  
			return;
		}
		else if(elem[i].value == '' && elem[i].getAttribute('required') == 'optional'){
			if(elem[i].value == ''){
				if(confirm('leave "'+elem[i].getAttribute('alias')+'" empty?')){ 
					k.style.backgroundColor='#ffeedd'; 
					k.value='NULL'; 
				}else { 
					k.style.backgroundColor='#ffeedd'; 
					k.focus(); 
					return;
				}
			}else{ 
				k.style.backgroundColor=''; 
			} 
		}
		
		if(elem[i].getAttribute('itype') == 'email'){
			if(elem[i].value.search("@") == -1 || elem[i].value.search("[.*]") == -1){
			   alert('Email address not valid');
			return false;
			}
		}
			
	qstr += elem[i].name + '=' + escape(elem[i].value) + '&';
	} 
	return qstr;
 }
function fileupload(form3,target,process,params){ 
var j = getId(form3);
if(j.elements[0].value){ 
j.action='?q='+process+'&file='+j.elements[0].id+'&folder='+j.elements[1].value+'&'+params;
j.target=target;
j.submit();
j.elements[0].value='';
}
 }
function remote_upload(form){ 
var j = getId(form);
var file = getInput(form,'file').value;
if(file){ 
createCookie('upload',file,1);
inLink('progress',file+' uploading...');
j.action='includes/inc.coldsis.php';
j.target='hidden_frame';
j.submit();
getInput(form,'file').value='';
}
 }
function getformvalues(url,dis,formId){ 
if(validate(formId)){
	if(dis!==null){ fadeIn('progress_bar',1000,1000); inLink('progress_bar','sending data...'); }
return validate(formId)+url;
}
 }
function link(str,url2,dis,gop,form3,urlX){
	//if(str === '') return alert('broken link');
	(urlX && urlX != '') ? X = urlX : X = 'lib/dankoff.php';
	var url = X;
	var display=dis;
    var xmlHttp3= new GetXmlHttpObject();
	if(xmlHttp3===null){ 
		alert('Browser does not support HTTP Request'); 
		return; 
	}
	if(str == 'reload'){
		(url2 == 'clear') ? killCookies() : eraseCookie('pc_main');
		window.location.reload(); 
		return; 
	} 
	(gop=='g') ? createCookie('pc_'+dis,str+','+url2+','+dis+','+gop+','+form3,'') : ''; 
	url=url+'?q='+str; 
	url=url+'&div='+dis+'&sid='+Math.random();	
	url=url+'&'+url2; 
		if(gop=='p'){
			if(getformvalues(url,dis,form3)){
			xmlHttp3.open('POST',url,true);
		    xmlHttp3.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded'); 
			xmlHttp3.send(getformvalues(url,dis,form3)); 
			openClose('dropDown','close');
			}
		}
		else if(gop=='g'){
			if(getId(dis)!==null){ setOpacity('progress_bar',10); inLink('progress_bar','please wait...'); }
			if(str.length===0){
			var di = getId(display);
			if(di!==null){di.innerHTML='';} 
			return; 
			}
			xmlHttp3.open('GET',url,true);
			xmlHttp3.send(null);
		}
    xmlHttp3.onreadystatechange = function(){ 
	var jp = getId(display);
	if(xmlHttp3.readyState==1){ if(jp!==null){ setOpacity('progress_bar',10); inLink('progress_bar','connecting...'); } }
	if(xmlHttp3.readyState==2){ if(jp!==null){ inLink('progress_bar','connected...'); } }
	if(xmlHttp3.readyState==3){ if(jp!==null){ inLink('progress_bar','downloading...'); } }
	if(xmlHttp3.readyState==4 || xmlHttp3.readyState=='complete'){
	/*if(xmlHttp3.status==404){ 
		alert('upgrade progress, please wait...'); 
		var y = self.setTimeout("window.location.reload()",10000);	
		return;
	}*/
	var tr = getId(display);
	if(tr!==null){
	var response = xmlHttp3.responseText;
	inLink('progress_bar','receiving data...');
	fadeOut('progress_bar',1000,1000);
		if(response && response !== ''){
			tr.innerHTML= response;
			tr.value=response;
		}
 } 
	}
 };
 }
function process_cookies(){
var whole_cookie = document.cookie;
var each_cookie = whole_cookie.split(";");  
for (i = 0; i < each_cookie.length; i++){ 
var info = each_cookie[i].split("=");  
var link_value = info[1];
var info3 = link_value.split(",");
	var a = info3[0];
	var b = info3[1];
	var c = info3[2];
	var d = info3[3];
	var e = info3[4];
if (a && a !=='' && c && c !==''){ 
	setTimeout("link('"+a+"','"+b+"&reload=no','"+c+"','g','"+e+"')",0); 
	}
 }
 }
function reLoad(){
	if(document.cookie && document.cookie !==""){ 
		process_cookies(); 
	} 
 }
/*function initialize(){
window.onload = new reLoad();
//var int = self.setInterval("link('notifications','reload=no','notificationArea','g','')",90000);
document.write('<iframe name="hidden_frame" id="hidden_frame" style="width:0; height:0; position:fixed; z-index:10; bottom:-10px; right:-10px;" frameborder="0" onLoad="if(readCookie(\'upload\')){inLink(\'progress\',readCookie(\'upload\')+\' upload complete!\');erase(\'upload\');}"></iframe>');
document.write('<div id="hidden_div" style="width:0; height:0; position:fixed; z-index:10; bottom:-10px; right:-10px;"></div>');
//var int = self.setInterval("repeat()",1000);
 }
initialize();
function repeat(){
if(getId('_mode') && getId('_mode').value == 'forum'){
link('forum','repeat&_mode='+getId('_mode').value+'&_type='+getId('_type').value+'&_parent='+getId('_parent').value+'&_name='+getId('_name').value,'chat-messages','g','');
getId('chat-messages').scrollTop = getId('chat-messages').scrollHeight;
}
 }*/
//toggle n stuff
function openClose(divName,state){
	if(state !== null || state !== ''){
		var j = getId(divName);
	if(j !== null){	
		if(state == 'open'){ 
			j.style.display = 'block'; } 
		else if(state == 'close'){ 
			j.style.display = 'none'; 
		}
	}
	}
 }
function toggle(div){
if(getId(div).style.display=='none'){ 
openClose(div,'open');
}else{ 
openClose(div,'close');
}
 }
function toggleHide(rC,div){
var j = getId(rC);
if(j.style.display=='none'){ 
j.style.display='block';
inLink(div,'Hide');
	}else{ 
j.style.display='none';
inLink(div,'Add New');
}
 }
function dropDown(event){
	document.getElementById('xyPoint').innerHTML='<span name="dropDown" id="dropDown" style="z-index:101; position:absolute; left:'+event.clientX+'px; top:'+event.clientY+'px"><img src="preload.gif"></span>';
 }
function remove(id,object,field){
	if(confirm('Do you really want to delete '+object+'?')){
		inLink('hidden_div','');
		eraseCookie('lastDeleted');
		createCookie('lastDeleted',id,1);
		link('update','elem=deleted&val=y&id='+id,'hidden_div','g','');
		if(getContent('hidden_div') === ''){ 
			openClose(field,'close'); 
			openClose(id,'close');	
		}
	}
	openClose('dropDown','close');
 }
function remove2(id,obj){
if(confirm('confirm delete?')){
inLink(id,(getContent(id)).replace(obj,''));
link('listLinks','string='+getContent('files'),'list_attachments','g','');
	}
 }
function toggle3(group,id,empty){
	if(document.cookie && readCookie(group) !== null && readCookie(group) !== ''){
		openClose(readCookie(group),'close');
		if(empty=='yes'){inLink(readCookie(group),'');}
		}
	openClose(id,'open');
	createCookie(group,id,1);
	}
function visited(id){
	if(document.cookie && readCookie('visited') !== null && readCookie('visited') !== ''){
		getId(readCookie('visited')).style.color='';
		}
	getId(id).style.color='#f00';
	createCookie('visited',id,1);
	}
function navigate(prefix,dir,div,left,right,num){ 
if(readCookie(num) === null || readCookie(num) === ''){createCookie(num,1,1);}
if(dir == 'left'){
createCookie(num,parseFloat(readCookie(num))-1,1);
inLink(div,getContent(prefix+readCookie(num)));
}else if(dir == 'right'){ 
createCookie(num,parseFloat(readCookie(num))+1,1);
inLink(div,getContent(prefix+readCookie(num)));
}else{
inLink(div,getContent(prefix+readCookie(num)));
}
if(getContent(prefix+'0') == 1){
openClose(left,'close'); openClose(right,'close');
}else if(readCookie(num) == 1){
openClose(left,'close'); openClose(right,'open');
}else if(readCookie(num) == getContent(prefix+'0')){
openClose(right,'close'); openClose(left,'open');
}else{
openClose(left,'open'); openClose(right,'open');
}
 }
function relocate(id){
if(!readCookie(id+'zindex')) createCookie(id+'zindex',2,1);
if(readCookie(id+'zindex') == 2){ 
getId(id).style.zIndex = 5;
createCookie(id+'zindex',5,1);
}else if(readCookie(id+'zindex') == 5){
getId(id).style.zIndex = 1;
createCookie(id+'zindex',1,1);
}else if(readCookie(id+'zindex') == 1){
getId(id).style.zIndex = 2;	
createCookie(id+'zindex',2,1);
}
 } //v4!
//edit
function copy(id){
	createCookie('clipboard',id,1);
	}
function paste(id){ 
var j = document.getElementById(id);
	if(document.cookie && readCookie('clipboard') !== null && readCookie('clipboard') !== ''){
			j.innerHTML=readCookie('clipboard');
			j.value=readCookie('clipboard');
	}
 }
function clear(){
	createCookie('clipboard','',1);
	}
//misc
function openClose2(value,valueX,id){
	if(value == valueX){
		openClose(id,'open');
		}else{
		openClose(id,'close');	
			}
	}
function clearfield(id){
getId(id).style.backgroundColor='';
 }
function swap(id,original,replacement){
var j = getId(id);
(j.innerHTML != original) ? j.innerHTML = original : j.innerHTML = replacement;
 }
function addUp(quantity,cost,total){
var qty = getId(quantity).value;	
var price = (getId(cost).value).replace(',','');	
getId(total).value = (qty * price).toFixed(2);
 }
function toPrice(amount,field){
var amt = (amount * 1).toFixed(2);
inLink(field,amt);	
	}
//move
function mobilize(id){ 
var j = getId(id); 
if(j.className == 'mobile'){
if(j.style.position == 'absolute'){
j.style.top = findPosY(j)+'px';
j.style.left = findPosX(j)+'px';
j.style.position = 'fixed';
j.style.zIndex = 0;
j.style.cursor = 'auto';
	}else{
j.style.position = 'absolute';
j.style.zIndex = 5;
j.style.top = findPosY(j)+'px';
j.style.left = findPosX(j)+'px';
j.style.cursor = 'move';
	}
}
	} 
//document.onmousedown=coordinates;
document.onmouseup=function(){document.onmousemove=null;};
document.ondblclick=function(){mobilize(readCookie('a-elem'));}
function coordinates(e){
if (e == null) { e = window.event;}
var sender = (typeof( window.event ) != "undefined" ) ? e.srcElement : e.target; 
createCookie('a-elem',sender.id,1);
var j = getId(readCookie('a-elem'));
mouseover=true;
a = parseInt(j.style.left)-e.clientX;
b = parseInt(j.style.top)-e.clientY;
if(j.className !== 'immobilize'){document.onmousemove=move;}
return false;
 }
function move(e){
if (e == null) { e = window.event;}
var j = getId(readCookie('a-elem'));
j.style.left = a+e.clientX+'px';
j.style.top = b+e.clientY+'px';
return false;
 }
//utilities
function findPosX(obj){
    var curleft = 0;
    if(obj.offsetParent)
        while(1) 
        {
          curleft += obj.offsetLeft;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.x)
        curleft += obj.x;
    return curleft;
  } 
function findPosY(obj){
    var curtop = 0;
    if(obj.offsetParent)
        while(1)
        {
          curtop += obj.offsetTop;
          if(!obj.offsetParent)
            break;
          obj = obj.offsetParent;
        }
    else if(obj.y)
        curtop += obj.y;
    return curtop;
 } 
//others
function enableDisable(id,state){
	var j = getId(id);
	if(state=='enable'){
		j.disabled=false;	
	}else if(state=='disable'){
		j.disabled=true; 
		}
	}
function clearComma(id){
var j = getId(id);
j.value = j.value.replace(',','');
 }
function dropDownInnerSelect(id){
var x = getId(id);
return x.options[x.selectedIndex].text;
	}
function insertAtCursor(myField,myValue){ //myField accepts an object reference, myValue accepts the text strint to add
//IE support
if (document.selection) {
myField.focus();
/*in effect we are creating a text range with zero length at the cursor location and replacing it with myValue*/
sel = document.selection.createRange();
sel.text = myValue;
}
//Mozilla/Firefox/Netscape 7+ support
else if (myField.selectionStart || myField.selectionStart == '0') {
/*Here we get the start and end points of the selection. Then we create substrings up to the start of the selection and from the end point of the selection to the end of the field value. Then we concatenate the first substring, myValue, and the second substring to get the new value.*/
var startPos = myField.selectionStart;
var endPos = myField.selectionEnd;
myField.value = myField.value.substring(0, startPos)+ myValue+ myField.value.substring(endPos, myField.value.length);
} else {
myField.value += myValue;
}
if(getId('files')){
openClose('dropDown','close');
link('listLinks','string='+getContent('files'),'list_attachments','g','');
}
 }
function resizeTextarea(id){
var j = getId(id); 
var text = j.value.length;
var area = j.rows * j.cols;
var ratio = text / area;
var invratio = 1/ratio;
if(ratio > 1){ ++j.rows; }
for(var x=0; x < 1000; x++){
	if(invratio >= 2){ --j.rows;
	} else { break;
	}
}
inLink('textrows',j.rows);
 }
 
  /*
Auto center window script- Eric King (http://redrival.com/eak/index.shtml)
Permission granted to Dynamic Drive to feature script in archive
For full source, usage terms, and 100's more DHTML scripts, visit http://dynamicdrive.com
*/


/////////////////////////////////////////////////////////////////////////////
var win = null;
function popOut(mypage,myname,w,h){
	w=900; h=670; //Default Valuses
LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
TopPosition = (screen.height) ? (screen.height-h)/4 : 0;
//settings ='height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable=no'
settings ='height=670,width=900,top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,resizable'
win = window.open(mypage,myname,settings)
}
/* onclick="popOut(this.href,'<?=date('Y');?> Class List Printing');return false;" */
