
	$(document).ready(function()
	{
		GETDATA();
		GETDATAAYEAR();
		GETSAVEDLECTURERS();
		GETSAVEVENUES();
		GETCLASSES();
		GETTA();
		editSESSION();
		EDITUSER();	
		$('#addCourse').click(SAVECOURSE);
		$('#ayear_save').click(SAVEAYEAR);
		$('#v_save').click(SAVEVENUE);	
		$('#l_save').click(SAVELECTURES);
		$('#c_save').click(SAVECLASSES);
		$('#s_ta').click(SAVETA);
		$('#ses_save').click(SAVESESSION);
		$('#u_save').click(SAVEUSERDATA);	
		$('#lec').click(CallCourses);
		$('#session').click(AllSessionCalls);
  	});
  	
  	
/*................. ..................COURSE FUNCTIONS...............................*/
	function displayCourse(id,cid,course,level)
	{
 	
 	$('#cname').val(course);
 	$('#cCode').val(cid)
 	$('#level').val(level);
 	$('#ccid').val(id);
 	$('#addCourse').html("Update");
    }
	function SAVECOURSE()
	{
		var cCode = $('#cCode').val();
		var cname = $('#cname').val();
		var level = $('#level').val();
		if(cCode !="" && cname !="" && level !="")
		{
			link("CREATECOURSE","","fcourse","p","courseID","models/dankoff.php");
		    
				setTimeout(function(){
					resetForm("cCode","cname","level","id");
					$('#fcourse').html("");
					$('#addCourse').html("SAVE");
					GETDATA();	
				},100);
		}
		else
		{
		displayDiv("fcourse","All Fields Required","error","3000");
		}
	}

	function GETDATA()
	{
		link("GETDATASAVED","","entData","g","","models/dankoff.php");
	}
	
	function DeleteCourse(id)
	{
		var cd = confirm("Are You Sure You Want To Delete");
		if(cd)
		{
		link('DELETIONCOURSE','idd='+id,'fcourse','g','','models/dankoff.php');				
				$('#entData').html("");
				setTimeout(function(){
					GETDATA();	
				},10) 

								
 		}	
	}
/*...................................ENDING OF COURSE...............................*/

/*......LECTURER FUNCTIONS........................*/
	function SAVELECTURES()
	{
		var lname = $('#lname').val();
		var lid = $('#lid').val();
		var dept = $('#dept').val();
		var cid = $('#cid').val();
		if(lname !="" && lid !="" && dept !="" && cid !="")
		{
			link("CREATELECTURERS","","lecfeed","p","frmlect","models/dankoff.php");
			setTimeout
			(
				function()
				{				
				resetForm("lname","lid","dept","idd","cid");
				$('#lecfeed').html("");
				$('#l_save').html("SAVE");
				GETSAVEDLECTURERS();
				},100
			)		
		
		}
		else
		{
			displayDiv("lecfeed","All Fields Required!!","error","2000");
		}
	}
	function GETSAVEDLECTURERS()
	{
	link("GETSAVELECTURERS","","lectData","g","","models/dankoff.php");
	}
	function displayLEC(ln,lid,de,cn,idd)
	{
	$('#lname').val(ln);
	$('#lid').val(lid);
	$('#dept').val(de);
	$('#cid').val(cn);
	$('#l_save').html("Update");
	$('#idd').val(idd);
	}
	function DeleteLEC(id)
	{
		var dlec = confirm("Are You Sure You Want To Delete");
		if(dlec)
		{
			link('DELETIONLEC','idd='+id ,'lecfeed','g','','models/dankoff.php');	
			setTimeout(function(){
				$('#lecfeed').html("");
				$('#lectData').html("");
				GETSAVEDLECTURERS();
			},1000);
		}
	}
	function CallCourses()
  	{  		
  		link("GETCOURSES","","cid","g","","models/dankoff.php");
  	}
/*......ENDING OF LECTURER FUNCTIONS........................*/

/*......ACADEMIC YEAR FUNCTIONS........................*/
	function displayAY(ay,ses)
	{
	$('#txtay').val(ay);
	$('#txtses').val(ses);
	$('#ayear_save').html("Update");
	}
	function OFFDIV(div,fields)
	{
	
	setTimeout(function(){
		$("#" + div).html("");
		resetForm(fields);

	},2000)
	}	
	function SAVEAYEAR()
	{
	var ay = $('#txtay').val();
	var ses = $('#txtses').val();
	if(ay && ses)
	{
		link("CREATEYEAR","","ayearfeed","p","fayear","models/dankoff.php");
		OFFDIV("ayearfeed",'');
		resetForm("txtay","txtses");
		GETDATAAYEAR();
	}
	else
	{
		displayDiv("ayearfeed","This Field is Required!!","error","3000");
		$('#ayear').focus();
	}
	}
	function GETDATAAYEAR()
	{
	link("GETAYEAR","","ayData","g","","models/dankoff.php");
	}

/*.................ENDING ACADEMIC YEAR FUNCTIONS....................*/


/*.................................VENUE FUNCTION...................................*/
	function SAVEVENUE()
	{
	var vid = $('#vID').val();
	var vname = $('#vname').val();
	if(vid !="" && vname !="")
	{
		link('CREATEVENUE','','vfeed','p','idvenue','models/dankoff.php');
		resetForm("vID",'vname','v_id');
		setTimeout(function(){
			$('#vfeed').html("");
			$('#v_save').html("SAVE");
			GETSAVEVENUES();
		},100);
		
	}
	else
	{
		displayDiv("vfeed","All Fields Required","error","2000");
	}
	}
	function GETSAVEVENUES()
	{
	link('GETSVENUE','','vData','g','','models/dankoff.php');
	}
	function displayVen(vid,vname,id)
	{
	 $('#vID').val(vid);
	 $('#vname').val(vname);
	  $('#v_id').val(id);
	  $('#v_save').html("Update");
	}
	function DeleteVen(id)
	{
	link('DELETIONVENUE','idd='+id,'vfeed','g','','models/dankoff.php');
	setTimeout(function(){
		$('#vfeed').html("");
		$('#vData').html("");
		GETSAVEVENUES();
	},1000)
	}
/*................................ENDING VENUE FUNCTION.............................*/
/*...................................CLASSES FUNCTIONS ................................*/
	function SAVECLASSES()
		{
		var cname = $('#classname').val();
		var level = $('#clevel').val();
		if(cname !="" && level !="")
		{
		link('CREATECLASS','','cfeed','p','fclasses','models/dankoff.php');
		setTimeout(function(){
			resetForm("classname","clevel","cidd");
			$('#cfeed').html("");
			$('#c_save').html("SAVE");
			GETCLASSES();
		},1000)
		}
		else
		{
			displayDiv("cfeed","All Fields Required!!","error","2000");
		}
		}
	function GETCLASSES()
		{
		link('GETSAVECLASS','','cData','g','','models/dankoff.php');
		}
	function DELETECLASS(id)
		{
			link('DELETECLASS','idd='+id,'cfeed','g','','models/dankoff.php');
			setTimeout(function(){
			$('#cfeed').html("");
			$('#cData').html("");
			GETCLASSES();
			},1000)

		}
	function EDITCLASS(cname,level,id)
		{
		 $('#classname').val(cname);
		 $('#clevel').val(level);
		 $('#cidd').val(id);		
		 $('#c_save').html("Update");		
		}
	function CLEAR(btn,frm,value)
	{		
		$('#'+ btn).html(value);
		clearme(frm);
	}
	function clearme(frmID)
	{
    $('#'+frmID).each(function(){
	        this.reset();		
	});
	}
/*....................................ENDING OF CLASSES FUNCTIONS......................*/
/*......................................TEACHING ASSIST................................*/
	function SAVETA()
	{
		var taid = $('#taID').val();
		var taname = $('#taname').val();
		if(taid !="" && taname !="")
		{
			link("CREATETA","","fta","p","frmta","models/dankoff.php");
			setTimeout(function(){
				$('#fta').html("");
				resetForm("taID","taname");
				$('s_ta').html("SAVE");
				GETTA();
			},100)
		} 
		else
		{
			displayDiv('fta',"All Fields Required!!","error","2000");
		}
	}
	function GETTA()
	{
		link("EDITTA","","taData","g","","models/dankoff.php");
	}
	function taEdit(id,tid,tname)
	{
		$('#taID').val(tid);
	    $('#taname').val(tname);
	    $('#dataID').val(id);
	    $('#s_ta').html("Update");
	}
	function taDelete(id)
	{
		link('DELETETA','taDD='+id,'fta','g','','models/dankoff.php');
		setTimeout(function(){
			$('#fta').html("");
			$('#taData').html("");
			GETTA();
		},1000)
	}
/*......................................ENDING TA......................................*/
/*.......................................SESSION FUNCTION..............................*/
	function SAVESESSION()
	{
		var ay = $('#sayear').val();
		var ss = $('#ss').val();
		var lid = $('#lID').val();
		var cCode = $('#ccode').val();
		var cname = $('#scname').val();
		var ve = $('#svenue').val();
		var ch = $('#chours').val();
		alert(ay);
		if(ay!="" && ss!="" && lid!="" && cCode!="" && cname!="" && ve!="" && ch!="")
		{
			link("CREATESESSIONS","","fses","p","frmSec","models/dankoff.php");
			setTimeout(function(){
				$('#fses').html("");
				$('#ses_save').html("SAVE");
				clearme('frmSec');
				editSESSION();
			},100)
		}
		else
		{
			displayDiv('fses','All Fields Required','error','2000');
		}
	}
	function editSESSION()
	{
      link("EDITSESSION","","sesData","g","","models/dankoff.php");
	}
	function sesEdit(id,ay,ss,ln,cc,cn,vn,ch)
	{
		 $('#sayear').val(ay);
		 $('#ss').val(ss);
		 $('#lID').val(ln);
		 $('#ccode').val(cc);
		 $('#scname').val(cn);
		 $('#svenue').val(vn);
		 $('#chours').val(ch);
		 $('#ses_save').html("Update");
		 
	}
	function sesDelete(id)
	{		
		var dome = confirm("Are You Sure You Want to Delete ");
		if(dome)
		{				
			link('DELETESESSION','ssID='+id,'fses','g','','models/dankoff.php');
			setTimeout(function(){
				$('#fses').html("");
				$('#sesData').html("");
				editSESSION();

			},100)
		}

	}
	function GetCoursesAtSession()
  	{  		
  		link("GETCOURSES","","ccode","g","","models/dankoff.php");
  	}
  	function CallLECTURERS()
  	{
  		link("GETLECTURER","","lID","g","","models/dankoff.php");
  	}
  	function CallSemester()
  	{
  		link("GETSEMESTER","","ss","g","","models/dankoff.php");
  	}
  	function CallClassname()
  	{
  		link("GETCLASSNAME","","scname","g","","models/dankoff.php");
  	}
  	function CallVenue()
  	{
  		link("GETVENUE","","svenue","g","","models/dankoff.php");
  	}
  	function AllSessionCalls()
  	{
  		GetCoursesAtSession();
  		CallLECTURERS();
  		CallSemester();
  		CallClassname();
  		CallVenue();
  	}

/*.......................................ENDING SESSION FUNCTION........................*/

/*......................................Create User.......................................*/
	function SAVEUSERDATA()
	{
		
		var ulname = $('#username').val();
		var ufon = $('#ufon').val();
		var uemail = $('#uemail').val();
		var cemail =  $('#cemail').val();
		var ups = $('#ups').val();
		var cps =  $('#cps').val();	
		if(ulname && ufon && uemail && cemail && ups && cps)			
		{
			if(uemail == cemail)
			{
				if(ups == cps)
				{
					link("CREATEUSER","","fuser","p","frmuser","models/dankoff.php");
					setTimeout(function(){
					$('#fuser').html("");
					$('#u_save').html("SAVE");
					},1000);
				}
				else
				{
					displayDiv("fuser","Password Does Not Match!!","error","2000");
					
				}
			}
			else
			{
				displayDiv("fuser","Email Does Not Match!!!","error","2000");
			}
			
		}
		else
		{
			displayDiv("fuser","All Fields Required","error","2000");
		}
		



	}

	function susAccount(id)
	{
		var susp = confirm("All You Sure You Want To Suspend This Account");
		if(susp)
		{
			link('SUSPENDACCOUNT','id='+id,'fuser','g','','models/dankoff.php');
			setTimeout(function(){
				//$('#r'+id).html("Suspended");
				$('#fuser').html("");
				EDITUSER();
				/*$('#t'+id).html("<img src='img/ac.jpg' width='40px'>")*/
			},100)
		}
	}
	function activateAcc(id)
	{
		var act = confirm("All You Sure You Want To Activate This Account");
		if(act)
		{
			link('ACTIVATEACCOUNT','id='+id,'fuser','g','','models/dankoff.php');
			setTimeout(function(){
				//$('#r'+id).html("Active");
				$('#fuser').html("");
				EDITUSER();
				/*$('#t'+id).html("<img src='img/ac.jpg' width='40px'>")*/
			},100)
		}
	}
	function EDITUSER()
	{
		link('GETDAUSSAVED','','userData','g','','models/dankoff.php');
	}

/*......................................Ending Of Create User..............................*/