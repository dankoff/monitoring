// Utility javascript file

function confirmEmails(email1, email2){
	if(email1.toString() == email2.toString())
		return true;
	return false;
}

function confirmPasswords(PWD, confPWD){
	if(PWD.toString() == confPWD.toString())
		return true;
	return false;
}

function isEmailValid(email){
	var flag = true;
	var emailreg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!emailreg.test(email)){
		flag = false;
	}
	return flag;
}

function resetForm(){
	for(var i = 0; i < arguments.length ; i++){
		$("#" + arguments[i]).val("");
	}
}

function showInfo(labelId, containerId, info, status)
{
	$('#' + labelId).html(info);
	$('#' + labelId).addClass(status);
	$('#' + containerId).css('display','block');
	setTimeout(function()
	{							
		$('#' + labelId).html("");
		$('#' + labelId).removeClass(status);
		$('#' + containerId).css('display','none');
	},3000);
}

function showAlert(labelId, containerId, info, status, seconds){
	$('#' + labelId).html(info);
	$('#' + containerId).addClass(status);
	$('#' + containerId).css('display','block');
	//$('#' + containerId).alert();
	setTimeout(function()
	{							
		$('#' + labelId).html("");
		$('#' + containerId).removeClass(status);
		$('#' + containerId).css('display','none');
		//$('#' + containerId).alert('close');
	},3000);
}

function showAlertIn(labelId, containerId, info, status, seconds){
	$('#' + labelId).html(info);
	$('#' + containerId).addClass(status);
	$('#' + containerId).css('display','block');
	//$('#' + containerId).alert();
	setTimeout(function()
	{							
		$('#' + labelId).html("");
		$('#' + containerId).removeClass(status);
		$('#' + containerId).css('display','none');
		//$('#' + containerId).alert('close');
	},seconds);
}

function selectComboElementText(id, text)
{
	$('#' + id + ' option').filter(function(){
		return $(this).text() == text;
	}).prop('selected', true);
}

function selectComboElementOption(id, text)
{
	$('#' + id + ' option').filter(function(){
		return $(this).val() == text;
	}).prop('selected', true);
}

function disableElement(id, flag){
	$("#" + id).prop('disabled',flag);
}
function IsFon(fon)
{
	
	var flag = true;
	var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/; 
	if(!phoneno.test(fon))
		{
		flag = false;
		}
	return flag;
	
}
function displayDiv(cont,info,status,seconds)
{
	$('#' + cont).html(info);
	$('#' + cont).addClass(status);
	setTimeout(function()
	{	
		$('#' + cont).html("");	
		$('#' + cont).removeClass(status);
	},seconds)
}
function displayOFF(div,seconds)
{
	setTimeout(function()
	{	
		$('#' + cont).html("");	
		$('#' + cont).removeClass(status);
	},seconds)
}



