<?php 
	include 'models/dankoff.php';

?>
<link rel="stylesheet" type="text/css" href="css/report.css">	
<div>
	<div>
		<h3>KOFORIDUA POLYTECHNIC</h3>
		<h4>QUALITY ASSURANCE DIRECTORATE</h4>
		<h5>MONITORING OF LECTURING ACTIVITIES</h5>
		<span>DATE: <?php echo gmdate("d / m / Y"); ?></span>
	</div>
	<div>
		<table border='1' id="monitory">
			<thead>
				<tr>
					<th>S/N</th>
					<th align="center">NAME OF LECTURER</th>
					<th>DEPT</th>
					<th>CLASS</th>
					<th>COURSE CODE OR TITLE</th>
					<th>VENUE</th>
					<th colspan='2' align="center">TIME</th>
					<th colspan='2' align="center">LECTURES IN SECTION</th>
					<th colspan='2' align="center">TAS <br/> ENGAGED IN TEACHING</th>
					<th>REMARKS</th>
				</tr>
				<tr>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th></th>
					<th>FROM</th>
					<th>TO</th>
					<th>YES</th>
					<th>NO</th>
					<th>YES</th>
					<th>NO</th>
					<th></th>
				</tr>				
			</thead>
			<tbody>
				<?php
					$get = mod_getData("select * from attendance");
					$nu = mod_nrows($get);
					if($nu !=0)
					{
						while ($data = mod_fetchObj($get))
						 {
							  $lecID = $data->lecturerID;
							 $class= $data->classname;							
							$dp = $data->dept;
							 $cc = $data->courseCode;
						
							$vn = $data->vanue;
							
							$tf = $data->starttime;
							$etime = $data->endingtime;
							$attended = $data->attended;							
							$tau = $data->TAUsed;
							$rmks = $data->comment;		
                             
				 			?>
								<tr>
									<td>1</td>
									<td><?php GETLECTURERNAME($lecID); ?></td>
									<td>Marketing</td>
									<td><?php echo $class; ?></td>
									<td><?php GETCOURSENAME($cc); ?></td>
									<td>Venue</td>
									<td><?php echo $tf; ?></td>
									<td><?php echo $etime;  ?></td>
									<td><?php if($attended == 1){echo"<img src='img/mk.jpg' width='30px'/>";}else{echo "";} ?></td>
									<td><?php if($attended == 0){echo"<img src='img/mk.jpg' width='30px'/>";} ?></td>
									<td><?php if($tau==1){echo"<img src='img/mk.jpg' width='30px'/>";} ?></td>
									<td><?php if($tau ==0){echo"<img src='img/mk.jpg' width='30px'/>";} ?></td>
									<td><?php echo $rmks; ?></td>
								</tr>
					 <?php 
						  }

					} 
					 ?>
			</tbody>
		</table>
	</div>
</div>